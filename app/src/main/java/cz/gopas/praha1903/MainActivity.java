package cz.gopas.praha1903;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

final public class MainActivity extends AppCompatActivity {

	private static final String TAG = MainActivity.class.getSimpleName();

	@BindView(R.id.op)
	RadioGroup op;
	@BindView(R.id.a)
	TextView aText;
	@BindView(R.id.b)
	TextView bText;
	@BindView(R.id.res)
	TextView res;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toast.makeText(this, "Create", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Create");

		ButterKnife.bind(this);
		op.setOnCheckedChangeListener((group, id) -> calc());

		try {
			Log.i(TAG, getIntent().getDataString());
		} catch (Throwable t) {
			//Ignore
		}
	}

	@OnClick(R.id.calc)
	void calc() {
		final Double a = Double.parseDouble(aText.getText().toString());
		final Double b = Double.parseDouble(bText.getText().toString());
		final Double result;
		switch (op.getCheckedRadioButtonId()) {
			case R.id.add:
				result = a + b;
				break;
			case R.id.sub:
				result = a - b;
				break;
			case R.id.mul:
				result = a * b;
				break;
			case R.id.div:
				result = a / b;
				break;
			default:
				result = Double.NaN;
				Toast.makeText(this, "Unknown operation", Toast.LENGTH_SHORT).show();
				Log.w(TAG, "Unknown operation");
		}
		res.setText(String.valueOf(result));
	}

	@OnClick(R.id.share)
	void share() {
		final Intent intent = new Intent(Intent.ACTION_SEND)
				.setType("text/plain")
				.putExtra(Intent.EXTRA_TEXT, res.getText().toString());
		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivity(intent);
		} else {
			Toast.makeText(this, getString(R.string.no_sharing_activity), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		//useless, because freezesText is true
		//outState.putString("result", res.getText().toString());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		//useless, because freezesText is true
		//res.setText(savedInstanceState.getString("result", ""));
	}

	@Override
	protected void onResume() {
		super.onResume();
		Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Resume");
	}

	@Override
	protected void onPause() {
		Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
		Log.d(TAG, "Pause");
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		Log.d(TAG, "Destroy");
		super.onDestroy();
	}
}
